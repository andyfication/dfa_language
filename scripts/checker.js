// This object validates and checks if the language provided by the user is correct 
class Cheker {
  constructor(nodes, network) {
    this.network = network;
    // List of nodes to analyze
    this.nodes = nodes;
  }
  // Initialise with task number one end highlighting the entry node
  initStartingPosition() {
    // Reset current info state to current node lable 
    $('#info-state').html('A');
    // Reset Graph Interaction 
    var options = {
      interaction: {
        selectable: true,
        hover: true
      }
    }
    this.network.setOptions(options);
    // Reset the graph colours
    for (let index = 1; index < this.nodes.length; index++) {
      if (this.nodes.get(index).accepted) {
        this.nodes.update({
          id: index,
          color: {
            background: 'rgb(40,40,40)',
            border: '#6fdc6f',
            highlight: {
              background: 'rgb(40,40,40)',
              border: '#6fdc6f'
            }
          }
        });
      } else {
        this.nodes.update({
          id: index,
          color: {
            background: 'rgb(40,40,40)',
            border: 'white',
            highlight: {
              background: 'rgb(40,40,40)',
              border: 'white'
            }
          }
        });
      }
    }
    this.nodes.update({
      id: 1,
      color: {
        border: 'white',
        background: '#ff9900'
      }
    });
  }
  // Validate the six strings, 3 accept strings and 3 reject strings
  validateLanguage(acceptFields, rejectFields) {
    // array containing the answers
    let answersAccept = [];
    let answersReject = [];
    // fill accept array with result 
    for (let index = 0; index < acceptFields.length; index++) {
      let result = initValidation($(acceptFields[index]).val());
      answersAccept.push(result.toString());
    }
    // fill reject array with result 
    for (let index = 0; index < rejectFields.length; index++) {
      let result = initValidation($(rejectFields[index]).val());
      answersReject.push(result.toString());
    }
    // If all answers are correct produce success output 
    if (answersAccept[0] == 'true' && answersAccept[1] == 'true' && answersAccept[2] == 'true' && answersReject[0] == 'false' && answersReject[1] == 'false' && answersReject[2] == 'false') {
      // All good mission complete 
      $('#suggestion').html("Well Done, you succesfully wrote a Langugae for this DFA <button class = 'button-next'>Reset</button>");
      $('#info-container-buttons-transparent').css('display', 'none');
      $('#info-container-buttons').addClass('highlightBorder');
      $('#info-container-buttons').removeClass('non-active');
      currentTutorialCount += 1;
      // Produce fail putput 
    } else {
      $('#info-container-buttons-transparent').css('display', 'none');
      $('#info-container-buttons').addClass('highlightBorder');
      $('#info-container-buttons').removeClass('non-active');
      // Highlight the strings that are not correct 
      for (let index = 0; index < acceptFields.length; index++) {
        if (answersAccept[index] != 'true') {
          $(acceptFields[index]).addClass('highlightError');
        }
      }
      for (let index = 0; index < rejectFields.length; index++) {
        if (answersReject[index] != 'false') {
          $(rejectFields[index]).addClass('highlightError');
        }
      }
      $('#suggestion').html("No Really, the Language you wrote is not valid for this DFA<button class = 'button-next'>Reset</button>");
      currentTutorialCount += 1;
    }
  }
}