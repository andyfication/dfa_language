//Main Javascript function. Handles the program 
$(document).ready(() => {
  let init = setInterval(() => {
    init_app(init);
  }, 100);
});

//This will be used for the coursera API
courseraApi.callMethod({
  type: "GET_SESSION_CONFIGURATION",
  onSuccess: init_coursera
});

function init_coursera() {
  let init = setInterval(() => {
    init_app(init);
  }, 100);
}

// Initialise the App
function init_app(init) {
  // if on main container, load the app
  if (initInterval) {
    // Initialise network with new nodes and options
    network = new vis.Network(container, generateNewGraph(0.7), options);
    // Initialise Head Node
    initEntryNode();
    // Initialise Rest of the Nodes
    initNodes();
    // Scale initial network
    network.once('stabilized', function () {
      var scaleOption = {
        scale: 1.2
      };
      network.moveTo(scaleOption);
    })
    // Create custom copy of the vis network for computation (the dfa object)
    initialiseNetwork();
    // Only make the graph interactive once positioned 
    let initGame = setInterval(() => {
      if (graphPositioned) {
        // these are all options in full.
        var options = {
          interaction: {
            dragNodes: false,
            dragView: true
          }
        }
        network.setOptions(options);
        // passing current nodes, tasks and the network to the checker 
        checker = new Cheker(nodes, network);
        // Init the checker position. Resets the head and other nodes
        checker.initStartingPosition();
        // Clear the initGame interval 
        clearInterval(initGame);
        // Reset the graph positioned variable 
        graphPositioned = false;
      }
    });
    //clear the interval for initApp. Only call it once after the intro animation
    clearInterval(init);
    // Event Handlers for node pointer 
    network.on("hoverNode", function (params) {
      network.canvas.body.container.style.cursor = 'pointer';
    });
    network.on("blurNode", function (params) {
      network.canvas.body.container.style.cursor = 'default';
    });

    // Event Handler for random graph ---------------------------------------------------------
    $('#info-random-graph-button').on('click', () => {
      // No longer suggesting to get a random string and grey out all sections except graph
      $('#info-container-buttons').removeClass('highlightBorder');
      $('#info-container-strings').removeClass('highlightBorder');
      $('#info-container-buttons-transparent').css('display', 'block');
      $('#info-container-strings-transparent').css('display', 'block');
      $('#info-container').addClass('non-active');
      // Set the right task
      currentTutorialCount = 6;
      tutorial[currentTutorialCount].set();
      // Initialise network with new nodes and options
      network = new vis.Network(container, generateNewGraph(0.7), options);
      // Initialise Head Node
      initEntryNode();
      // Initialise Rest of the Nodes
      initNodes();
      // Scale initial network
      network.once('stabilized', function () {
        var scaleOption = {
          scale: 1.2
        };
        network.moveTo(scaleOption);
      })
      // Create custom copy of the vis network for computation. The dfa object 
      initialiseNetwork();
      // Only make the graph interactive once positioned 
      let initGame = setInterval(() => {
        if (graphPositioned) {
          // these are all options in full.
          var options = {
            interaction: {
              dragNodes: false,
              dragView: true
            }
          }
          network.setOptions(options);
          // passing current nodes and the network to the checker 
          checker = new Cheker(nodes, network);
          // Init the checker position. Reset head and rest of the nodes
          checker.initStartingPosition();
          // Clear the interval 
          clearInterval(initGame);
          // Reset the graph positioned variable
          graphPositioned = false;
        }
      });
    });
  }
}