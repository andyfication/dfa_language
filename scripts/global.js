// Global variables
let introInterval;
let initInterval = false;
let currentTutorialCount = 0;
let graphPositioned = false;
var network;
var nodes;
var edges;
var data;
var checker;
var tutorial;

$(document).ready(() => {

  // Tutorial Object, has text to display and a function to change the state of the content
  tutorial = [{
      text: "Welcome, I'll be your Task Manager and Guide <button class = 'button-next'> Next</button> <button class = 'button-skip'>Skip</button>",
      set: function () {
        $('#info-container').addClass('non-active');
        $('#canvas-container').addClass('non-active');
        $('#reject-string-container').addClass('non-active');
        $('#accept-string-container').addClass('non-active');
        $('#info-container-transparent').css('display', 'block');
        $('#canvas-container-transparent').css('display', 'block');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Here you can write your Automaton's Language <button class = 'button-next'>Next</button>",
      set: function () {
        $('#info-container-transparent').css('display', 'none');
        $('#info-container-strings').addClass('highlightBorder');
        $('#info-container-buttons-transparent').css('display', 'block');
        $('#accept-string-container-transparent').css('display', 'none');
        $('#reject-string-container-transparent').css('display', 'none');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Here you can generate new random Graphs<button class = 'button-next'>Next</button>",
      set: function () {
        $('#info-container-strings').removeClass('highlightBorder');
        $('#info-container-buttons').addClass('highlightBorder');
        $('#info-container-strings-transparent').css('display', 'block');
        $('#info-container-buttons-transparent').css('display', 'none');
        $('#accept-string-container-transparent').css('display', 'block');
        $('#reject-string-container-transparent').css('display', 'block');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Here you can manipulate the Graph and check its legend  <button class = 'button-next'>Next</button>",
      set: function () {
        $('#info-container-buttons').removeClass('highlightBorder');
        $('#canvas-container').addClass('highlightBorder');
        $('#main-info-container').addClass('highlightBorder');
        $('#info-container-buttons-transparent').css('display', 'block');
        $('#canvas-container-transparent').css('display', 'none');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Finally you can use the [Help] button to review information <button class = 'button-next'>Next</button>",
      set: function () {
        $('#canvas-container').removeClass('highlightBorder');
        $('#main-info-container').removeClass('highlightBorder');
        $('#help').addClass('highlightBorder');
        $('#canvas-container-transparent').css('display', 'block');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Great, I think you are ready to go !<button class = 'button-next'>Begin</button>",
      set: function () {
        $('#help').removeClass('highlightBorder');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Position and organise the Graph so that it is clear to you <button class = 'button-next'>Next</button>",
      set: function () {
        let acceptFields = $('.accept-fields');
        let rejectFields = $('.reject-fields');
        for (let index = 0; index < acceptFields.length; index++) {
          $(acceptFields[index]).removeClass('highlightError');
          $(rejectFields[index]).removeClass('highlightError');
        }
        $('#help').removeClass('highlightBorder');
        $('#accept-string-container-transparent').css('display', 'block');
        $('#reject-string-container-transparent').css('display', 'block');
        $('#info-container-buttons').removeClass('non-active');
        $('#canvas-container').removeClass('non-active');
        $('#canvas-container-transparent').css('display', 'none');
        $('#accept-string-container').removeClass('highlightBorder');
        $('#reject-string-container').removeClass('highlightBorder');
        let accFields = $('.accept-fields');
        let rejFields = $('.reject-fields');
        for (let index = 0; index < accFields.length; index++) {
          $(accFields[index]).val('');
          $(rejFields[index]).val('');
        }
        $('#suggestion').html(this.text);
        var options = {
          interaction: {
            dragNodes: true,
            dragView: true
          }
        }
        network.setOptions(options);
      }
    },
    {
      text: "Write 3 valid Binary Strings accepted by this DFA <button class = 'button-next'>Next</button>",
      set: function () {
        let acceptFields = $('.accept-fields');
        let rejectFields = $('.reject-fields');
        for (let index = 0; index < acceptFields.length; index++) {
          $(acceptFields[index]).removeClass('highlightError');
          $(rejectFields[index]).removeClass('highlightError');
        }
        $('#info-container').removeClass('non-active');
        $('#info-container-transparent').css('display', 'none');
        $('#info-container-buttons-transparent').css('display', 'none');
        $('#info-container-strings-transparent').css('display', 'none');
        $('#canvas-container-transparent').css('display', 'none');
        $('#accept-string-container').removeClass('non-active');
        $('#accept-string-container-transparent').css('display', 'none');
        $('#reject-string-container-transparent').css('display', 'block');
        $('#accept-string-container').addClass('highlightBorder');
        $('#info-container-buttons').removeClass('highlightBorder');
        $('#suggestion').html(this.text);
        var options = {
          interaction: {
            dragNodes: false,
            dragView: true
          }
        }
        network.setOptions(options);
      }
    },
    {
      text: "Write 3 valid Binary Strings rejected by this DFA <button class = 'button-next'>Next</button>",
      set: function () {
        $('#info-container').removeClass('non-active');
        $('#info-container-transparent').css('display', 'none');
        $('#info-container-buttons-transparent').css('display', 'none');
        $('#info-container-strings-transparent').css('display', 'none');
        $('#canvas-container-transparent').css('display', 'none');
        $('#accept-string-container-transparent').css('display', 'block');
        $('#accept-string-container').removeClass('highlightBorder');
        $('#reject-string-container').removeClass('non-active');
        $('#reject-string-container-transparent').css('display', 'none');
        $('#reject-string-container').addClass('highlightBorder');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Great, you can now validate your DFA Language<button class = 'button-next'>Validate</button>",
      set: function () {
        $('#reject-string-container').addClass('non-active');
        $('#accept-string-container').addClass('non-active');
        $('#info-container-buttons').addClass('non-active');
        $('#info-container-transparent').css('display', 'none');
        $('#info-container-buttons-transparent').css('display', 'block');
        $('#info-container-strings-transparent').css('display', 'none');
        $('#canvas-container-transparent').css('display', 'none');
        $('#accept-string-container-transparent').css('display', 'none');
        $('#accept-string-container').removeClass('highlightBorder');
        $('#reject-string-container-transparent').css('display', 'none');
        $('#reject-string-container').removeClass('highlightBorder');
        $('#suggestion').html(this.text);
      }
    }
  ];

  // Setting the first tutorial task
  tutorial[currentTutorialCount].set();

  // Increment tutorial task on button click 
  $('#suggestion').on('click', '.button-next', () => {
    // Last tutorial task before the game 
    if (currentTutorialCount == 6) {
      graphPositioned = true;
      currentTutorialCount += 1;
      tutorial[currentTutorialCount].set();
      // Validate the accept strings
    } else if (currentTutorialCount == 7) {
      validateAcceptedStrings();
      // Validate the reject strings
    } else if (currentTutorialCount == 8) {
      validateRejectedStrings();
      // Validate all strings together and produce answer 
    } else if (currentTutorialCount == 9) {
      let fieldsAcc = $('.accept-fields');
      let fieldsRej = $('.reject-fields');
      checker.validateLanguage(fieldsAcc, fieldsRej);
      // Go back to the beginning of the game 
    } else if (currentTutorialCount == 10) {
      currentTutorialCount = 7;
      tutorial[currentTutorialCount].set();
      // Increase the task manager 
    } else {
      currentTutorialCount += 1;
      tutorial[currentTutorialCount].set();
    }
  });
  //Jump the tutorial section 
  $('#suggestion').on('click', '.button-skip', () => {
    currentTutorialCount = 6;
    tutorial[currentTutorialCount].set();
  });
  // Toggle the Graph Legenda on help button click 
  $('#help').on('click', () => {
    $(".modal-title").html('Graph Legend and Language');
    $(".modal-body").html('<h3 style="margin-top:30px">Actions</h3><p> The <strong>Graph</strong> is fully interactive and you can :</p> <p><strong>Position the Graph :</strong> Click one of the edges and drag</p> <p><strong>Zoom in and Out :</strong> Use the mouse wheel</p> <p><strong>Select Nodes : </strong> Click the desidered node</p> <p style="margin-bottom:40px"><strong>Drag Nodes :</strong> Click and drag the desidered node</p> <h3>Nodes</h3> <p style = "font-size:20px"> Current Node <span id="info-state">A</span></p> <p style = "font-size:20px"> Nodes with a <strong>Reject State</strong> <span id="reject-state">A</span></p><p style = "font-size:20px"> Nodes with an <strong>Accept State</strong> <span id="accept-state">A</span></p>  <h3>Edges</h3> <p style = "font-size:20px"> Edge/Transition <img src = "images/arrow.gif" id ="arrow"/> </p> <p style = "font-size:20px"> Edge Value <img src = "images/arrow_label.gif" id ="arrow-label"/> </p> <br /> <h3>Automaton\'s Language</h3> <br /> <p style = "font-size:20px"> Check your <strong>Current State </strong> <br /><br /> Identify all the <strong>Nodes</strong> with an <strong>Accept State</strong><br /><br />  Write 3 <strong>Binary Strings</strong> accepted by this <strong>DFA</strong> <br /> <br /> Write 3 <strong>Binary Strings</strong> rejected by this <strong>DFA</strong> <br /> <br /> Check the <strong>DFA</strong> edges value to determine<br /> the path that your strings should follow<br /> to be either <strong>accepted</strong> or <strong>rejected</strong> <br /> <br /> If there is no solution for a particular <strong>DFA</strong>,<br /> just hit the <strong>Random Graph</strong> button to <br /> generate a new one');
    $(".modal").modal("show");
  });

  // Helper functions to validate the string input
  function validateAcceptedStrings() {
    let fields = $('.accept-fields');
    let allFieldsFilled = true;
    // check if there are empty fields
    for (let index = 0; index < fields.length; index++) {
      if (!$(fields[index]).val()) {
        $('#suggestion').html("Make sure you fill in all the fields<button class = 'button-next'>Next</button>");
        allFieldsFilled = false;
        break;
      }
    }
    // Check that if all the fields are filled, they are filled with 0 and 1
    if (allFieldsFilled) {
      if ($(fields[0]).val() && $(fields[1]).val() && $(fields[2]).val()) {
        let firstValue = $(fields[0]).val();
        let secondValue = $(fields[1]).val();
        let thirdValue = $(fields[2]).val();
        // Check for binary string repetition 
        if (isBinaryInput(firstValue) && isBinaryInput(secondValue) && isBinaryInput(thirdValue)) {
          if ($(fields[0]).val() == $(fields[1]).val() || $(fields[0]).val() == $(fields[2]).val() || $(fields[1]).val() == $(fields[2]).val()) {
            $('#suggestion').html("Please do not repeat the same String<button class = 'button-next'>Next</button>");
          } else {
            currentTutorialCount += 1;
            tutorial[currentTutorialCount].set();
          }
        } else {
          $('#suggestion').html("Please enter valid Binary Strings, yes only zeroes and ones <button class = 'button-next'>Next</button>");
        }
      }
    }
  }
  // Helper functions to validate the string input
  function validateRejectedStrings() {
    let fields = $('.reject-fields');
    let allFieldsFilled = true;
    // check if there are empty fields
    for (let index = 0; index < fields.length; index++) {
      if (!$(fields[index]).val()) {
        $('#suggestion').html("Make sure you fill in all the fields<button class = 'button-next'>Next</button>");
        allFieldsFilled = false;
        break;
      }
    }
    // Check that if all the fields are filled, they are filled with 0 and 1
    if (allFieldsFilled) {
      if ($(fields[0]).val() && $(fields[1]).val() && $(fields[2]).val()) {
        let firstValue = $(fields[0]).val();
        let secondValue = $(fields[1]).val();
        let thirdValue = $(fields[2]).val();
        // Check for binary string repetition 
        if (isBinaryInput(firstValue) && isBinaryInput(secondValue) && isBinaryInput(thirdValue)) {
          if ($(fields[0]).val() == $(fields[1]).val() || $(fields[0]).val() == $(fields[2]).val() || $(fields[1]).val() == $(fields[2]).val()) {
            $('#suggestion').html("Please do not repeat the same String<button class = 'button-next'>Next</button>");
          } else {
            currentTutorialCount += 1;
            tutorial[currentTutorialCount].set();
          }
        } else {
          $('#suggestion').html("Please enter valid Binary Strings, yes only zeroes and ones <button class = 'button-next'>Next</button>");
        }
      }
    }
  }
  // check for a binary input
  function isBinaryInput(str) {
    for (let index = 0; index < str.length; index++) {
      if (str[index] != "0" && str[index] != "1") {
        return false;
      }
    }
    return true;
  }
});