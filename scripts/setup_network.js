// Grab the container variable 
var container = $('#network_canvas')[0];
// Set Data options 
var options = {
  interaction: {
    hover: true
  },
  edges: {
    arrows: {
      to: {
        enabled: true,
        scaleFactor: 0.8,
        type: 'arrow'
      }
    },
    color: {
      color: '#00FFFF',
      highlight: '#848484',
      hover: '#848484',
      inherit: 'from',
      opacity: 0.5
    },
    chosen: false
  },
  layout: {
    improvedLayout: true,
    hierarchical: {
      enabled: false
    }
  }
};
// Function to initialise the entry node 
function initEntryNode() {
  nodes.update({
    id: -1,
    chosen: false,
    color: {
      border: 'rgb(40,40,40)',
      background: 'red',
    },
    shape: 'square',
    size: 5,
    font: {
      color: 'white'
    },
    font: {
      color: 'white',
      size: 20, // px
      face: 'arial',
      background: 'none',
      align: 'center'
    },
    physics: false
  });
}
// function to initialise the rest of the nodes
function initNodes() {
  for (let index = 1; index < nodes.length; index++) {

    if (!nodes.get(index).accepted) {

      nodes.update({
        id: index,
        chosen: true,
        borderWidth: 2,
        color: {
          border: 'white',
          background: 'rgb(40,40,40)',
          highlight: {
            border: 'rgb(40,40,40)',
            background: '#ff9900'
          },
          hover: {
            border: 'rgb(40,40,40)',
            background: 'green'
          }
        },
        shape: 'box',
        scaling: {
          label: {
            enabled: true,
            min: 35,
            max: 35
          }
        },
        value: 1,
        font: {
          color: 'white'
        },
        font: {
          color: 'white',
          size: 10, // px
          face: 'arial',
          background: 'none',
          align: 'center'
        },
        physics: false
      });
    } else {
      nodes.update({
        id: index,
        chosen: true,
        borderWidth: 2,
        color: {
          border: '#6fdc6f',
          background: 'rgb(40,40,40)',
          highlight: {
            border: 'rgb(40,40,40)',
            background: '#ff9900'
          },
          hover: {
            border: 'rgb(40,40,40)',
            background: 'green'
          }
        },
        shape: 'ellipse',
        scaling: {
          label: {
            enabled: true,
            min: 35,
            max: 35
          }
        },
        value: 1,
        font: {
          color: 'white'
        },
        font: {
          color: 'white',
          size: 10, // px
          face: 'arial',
          background: 'none',
          align: 'center'
        },
        physics: false
      });
    }
  }
}