This is my repo for the Deterministic Finite Automaton Language project.


I worked with Goldsmiths, University of London and Coursera to develop some online interactive learning tools. The Deterministic Finite Automaton Language is among these.

The DFA Language is an interactive simulation made in HTML5,CSS3 and javaScript.

The plugin shows how DFA is used in combination of binary strings to establish accepted or rejected strings known as the Language of the DFA.

